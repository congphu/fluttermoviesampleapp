import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_sample_app/src/blocs/movie_list/movie_bloc.dart';
import 'package:movie_sample_app/src/repositories/movie_repository.dart';
import 'package:movie_sample_app/src/ui/screens/movie_list/movie_list_screen.dart';

void main() {
  runApp(
    BlocProvider(
      create: (context) => MovieBloc(DefaultMovieRepository()),
      child: const MovieListScreen(),
    ),
  );
}
