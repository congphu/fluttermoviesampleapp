import 'package:dio/dio.dart';
import 'package:dio_logging_interceptor/dio_logging_interceptor.dart';

import '../models/trailer_list.dart';
import '../models/trending_list.dart';
import '../network/api_client.dart';
import '../network/interceptors/authentication_interceptor.dart';

abstract class MovieRepository {
  Future<TrendingList> getListMovies(int page);
  Future<TrailerList> getTrailer(int movieId);
}

class DefaultMovieRepository implements MovieRepository {
  DefaultMovieRepository() {
    final dio = Dio();
    _apiClient = ApiClient(dio);
  }
  late ApiClient _apiClient;

  @override
  Future<TrendingList> getListMovies(int page) {
    return _apiClient.getMovies(page);
  }

  @override
  Future<TrailerList> getTrailer(int movieId) {
    return _apiClient.getTrailer(movieId);
  }
}

class NetworkError extends Error {
  String errorMessage;

  NetworkError({required this.errorMessage});
}
