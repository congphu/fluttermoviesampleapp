import 'package:dio/dio.dart';
import 'package:dio_logging_interceptor/dio_logging_interceptor.dart';
import 'package:movie_sample_app/src/constants/api_constants.dart';
import 'package:movie_sample_app/src/models/trailer_list.dart';
import 'package:retrofit/retrofit.dart';
import '../models/trending_list.dart';
import 'interceptors/authentication_interceptor.dart';
part 'api_client.g.dart';

@RestApi(baseUrl: ApiConstants.baseUrl)
abstract class ApiClient {
  factory ApiClient(Dio dio) {
    dio.options = BaseOptions(
        receiveTimeout: ApiConstants.timeout,
        connectTimeout: ApiConstants.timeout);
    dio.interceptors.add(AuthenticationInterceptor());
    dio.interceptors.add(DioLoggingInterceptor(level: Level.body));
    return _ApiClient(dio, baseUrl: ApiConstants.baseUrl);
  }

  @GET("/trending/all/day")
  Future<TrendingList> getMovies(@Query("page") int page);

  @GET("/movie/{movieId}/videos")
  Future<TrailerList> getTrailer(@Path("movieId") int movieId);
}
