import 'package:dio/dio.dart';

class ApiError implements Exception {
  late int? _errorCode;
  String _errorMessage = "";

  ApiError(DioError error) {
    _handleError(error);
  }

  getErrorCode() {
    return _errorCode;
  }

  getErrorMessage() {
    return _errorMessage;
  }

  _handleError(DioError error) {
    switch (error.type) {
      case DioErrorType.cancel:
        _errorMessage = "Request was cancelled";
        break;
      case DioErrorType.connectTimeout:
        _errorMessage = "Connection timeout";
        break;
      case DioErrorType.other:
        _errorMessage = "Connection failed due to internet connection";
        break;
      case DioErrorType.receiveTimeout:
        _errorMessage = "Receive timeout in connection";
        break;
      case DioErrorType.response:
        _errorCode = error.response?.statusCode;
        _errorMessage =
            "Received invalid response with status code: ${error.response?.statusCode}";
        break;
      case DioErrorType.sendTimeout:
        _errorMessage = "Receive timeout in sending request";
        break;
    }
    return _errorMessage;
  }
}
