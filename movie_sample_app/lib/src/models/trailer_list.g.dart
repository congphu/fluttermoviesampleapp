// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trailer_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrailerList _$TrailerListFromJson(Map<String, dynamic> json) => TrailerList(
      json['id'] as int,
      (json['results'] as List<dynamic>)
          .map((e) => TrailerModel.fromJson(e))
          .toList(),
    );

Map<String, dynamic> _$TrailerListToJson(TrailerList instance) =>
    <String, dynamic>{
      'id': instance.id,
      'results': instance.trailers,
    };
