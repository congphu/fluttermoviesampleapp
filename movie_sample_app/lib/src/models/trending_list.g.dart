// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trending_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrendingList _$TrendingListFromJson(Map<String, dynamic> json) => TrendingList(
      json['page'] as int,
      (json['results'] as List<dynamic>)
          .map((e) => Movie.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total_pages'] as int,
      json['total_results'] as int,
    );

Map<String, dynamic> _$TrendingListToJson(TrendingList instance) =>
    <String, dynamic>{
      'page': instance.page,
      'results': instance.movies,
      'total_pages': instance.totalPages,
      'total_results': instance.totalResults,
    };
