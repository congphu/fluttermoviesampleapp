import 'package:json_annotation/json_annotation.dart';
import 'package:movie_sample_app/src/models/trailer_model.dart';

part 'trailer_list.g.dart';

@JsonSerializable()
class TrailerList {
  int id;

  @JsonKey(name: 'results')
  List<TrailerModel> trailers;

  TrailerList(this.id, this.trailers);

  factory TrailerList.fromJson(json) => _$TrailerListFromJson(json);
}
