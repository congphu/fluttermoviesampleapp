import 'package:json_annotation/json_annotation.dart';
part 'movie.g.dart';

@JsonSerializable()
class Movie {
  int id;
  String? title;

  @JsonKey(name: 'original_title')
  String? originalTitle;

  @JsonKey(name: 'poster_path')
  String? posterPath;

  String? overview;

  @JsonKey(name: 'release_date')
  String? releaseDate;

  @JsonKey(name: 'vote_count')
  int voteCount;

  @JsonKey(name: 'vote_average')
  double? rating;

  Movie(this.id, this.title, this.originalTitle, this.posterPath, this.overview,
      this.releaseDate, this.voteCount, this.rating);

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);

  String get safeTitle => title ?? originalTitle ?? '';
}
