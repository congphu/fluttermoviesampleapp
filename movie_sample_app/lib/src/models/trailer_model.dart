import 'package:json_annotation/json_annotation.dart';

part 'trailer_model.g.dart';

@JsonSerializable()
class TrailerModel {
  String id;

  @JsonKey(name: 'iso_639_1')
  String? iso639;

  @JsonKey(name: 'iso_3166_1')
  String? iso3166;
  String? key;
  String? name;
  String? site;
  int size;
  String? type;

  TrailerModel(this.id, this.iso639, this.iso3166, this.key, this.name,
      this.site, this.size, this.type);

  factory TrailerModel.fromJson(json) => _$TrailerModelFromJson(json);
}
