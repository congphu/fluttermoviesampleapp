import 'package:json_annotation/json_annotation.dart';
import 'package:movie_sample_app/src/models/movie.dart';

part 'trending_list.g.dart';

@JsonSerializable()
class TrendingList {
  int page;

  @JsonKey(name: 'results')
  List<Movie> movies;

  @JsonKey(name: 'total_pages')
  int totalPages;

  @JsonKey(name: 'total_results')
  int totalResults;

  TrendingList(this.page, this.movies, this.totalPages, this.totalResults);

  factory TrendingList.fromJson(json) => _$TrendingListFromJson(json);
}
