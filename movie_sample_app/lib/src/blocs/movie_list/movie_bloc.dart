import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

import '../../models/movie.dart';
import '../../repositories/movie_repository.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final MovieRepository repository;
  int _currentPage = 1;
  bool _hasMore = true;
  bool _isLoading = false;
  List<Movie> _movies = [];

  MovieBloc(this.repository) : super(MovieInitial()) {
    on<MovieEvent>(_onEvent);
  }

  void _onEvent(
    MovieEvent event,
    Emitter<MovieState> emitter,
  ) async {
    if (event is RefreshMovies) {
      _resetData();
    }
    if (!_hasMore || _isLoading) {
      return;
    }
    _isLoading = true;
    try {
      final movieList = await repository.getListMovies(_currentPage);
      _isLoading = false;
      _hasMore = 2 > _currentPage;
      _currentPage++;
      _movies.addAll(movieList.movies);
      emitter(
        MovieGetDataSuccess(
          _movies.toList(),
          _hasMore,
        ),
      );
    } catch (e) {
      _isLoading = false;
      _emitError(error: e, emitter: emitter);
    }
  }

  void _emitError(
      {required Object error, required Emitter<MovieState> emitter}) {
    if (error is NetworkError) {
      emitter(
        MovieGetDataError(
          error.errorMessage,
        ),
      );
    }
  }

  void _resetData() {
    _hasMore = true;
    _isLoading = false;
    _currentPage = 1;
    _movies = [];
  }
}
