part of 'movie_bloc.dart';

abstract class MovieEvent extends Equatable {
  const MovieEvent();

  @override
  List<Object> get props => [];
}

class GetMovies extends MovieEvent {
  const GetMovies();
}

class RefreshMovies extends MovieEvent {
  const RefreshMovies();
}
