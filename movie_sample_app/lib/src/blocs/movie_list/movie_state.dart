part of 'movie_bloc.dart';

abstract class MovieState extends Equatable {
  const MovieState();

  @override
  List<Object> get props => [];
}

class MovieInitial extends MovieState {}

class MovieGetDataSuccess extends MovieState {
  final List<Movie> movies;
  final bool hasMore;

  const MovieGetDataSuccess(this.movies, this.hasMore);
  @override
  List<Object> get props => [movies.length];
}

class MovieGetDataError extends MovieState {
  final String message;

  const MovieGetDataError(this.message);
  @override
  List<Object> get props => [message];
}
