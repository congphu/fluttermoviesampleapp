import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_sample_app/src/network/api_error.dart';
import '../../models/trailer_model.dart';
import '../../repositories/movie_repository.dart';
import 'package:dio/dio.dart';

part 'movie_detail_event.dart';
part 'movie_detail_state.dart';

class MovieDetailBloc extends Bloc<MovieDetailBlocEvent, MovieDetailState> {
  final MovieRepository repository;
  MovieDetailBloc(this.repository) : super(MovieDetailInitial()) {
    on<MovieDetailBlocEvent>((event, emit) async {
      try {
        final trailerList = await repository.getTrailer(event.movieId);
        emit(MovieDetailGetDataSuccess(trailerList.trailers));
      } catch (e) {
        _emitError(error: e, emitter: emit);
      }
    });
  }

  void _emitError(
      {required Object error, required Emitter<MovieDetailState> emitter}) {
    if (error is DioError) {
      emitter(
        MovieDetailGetDataError(
          ApiError(error).getErrorMessage(),
        ),
      );
    }
  }
}
