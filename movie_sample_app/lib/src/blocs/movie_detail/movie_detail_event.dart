part of 'movie_detail_bloc.dart';

class MovieDetailBlocEvent extends Equatable {
  final int movieId;
  const MovieDetailBlocEvent({required this.movieId});

  @override
  List<Object> get props => [];
}
