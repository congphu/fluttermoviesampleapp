part of 'movie_detail_bloc.dart';

abstract class MovieDetailState extends Equatable {
  const MovieDetailState();

  @override
  List<Object> get props => [];
}

class MovieDetailInitial extends MovieDetailState {}

class MovieDetailGetDataSuccess extends MovieDetailState {
  final List<TrailerModel> trailers;

  const MovieDetailGetDataSuccess(this.trailers);
}

class MovieDetailGetDataError extends MovieDetailState {
  final String message;

  const MovieDetailGetDataError(this.message);
  @override
  List<Object> get props => [message];
}
