import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/movie_detail/movie_detail_bloc.dart';
import '../../../models/movie.dart';
import '../../../models/trailer_model.dart';

class MovieDetailScreen extends StatefulWidget {
  final Movie movie;
  const MovieDetailScreen(this.movie, {super.key});

  @override
  State<MovieDetailScreen> createState() => _MovieDetailScreenState();
}

class _MovieDetailScreenState extends State<MovieDetailScreen> {
  late MovieDetailBloc bloc;
  bool _isSnackbarActive = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = context.read<MovieDetailBloc>();
    bloc.add(MovieDetailBlocEvent(movieId: widget.movie.id));
  }

  @override
  Widget build(BuildContext context) {
    final movie = widget.movie;
    return WillPopScope(
      onWillPop: () {
        if (_isSnackbarActive) {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        }
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: Scaffold(
        body: SafeArea(
          top: false,
          bottom: false,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  expandedHeight: 300.0,
                  floating: true,
                  pinned: true,
                  elevation: 0.0,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Image.network(
                      "https://image.tmdb.org/t/p/w500${movie.posterPath}",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ];
            },
            body: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    movie.safeTitle,
                    style: const TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
                  Text(movie.overview ?? ''),
                  Container(
                      margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
                  const Text(
                    "Trailer",
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.only(top: 8.0, bottom: 8.0)),
                  BlocConsumer<MovieDetailBloc, MovieDetailState>(
                    builder: (context, state) {
                      if (state is MovieDetailInitial) {
                        return const Center(child: CircularProgressIndicator());
                      }
                      if (state is MovieDetailGetDataSuccess) {
                        if (state.trailers.isNotEmpty) {
                          return trailerLayout(state.trailers);
                        }
                        return noTrailer();
                      }
                      return Container();
                    },
                    listener: (context, state) {
                      if (state is MovieDetailGetDataError) {
                        _isSnackbarActive = true;
                        final snackBar = SnackBar(content: Text(state.message));
                        ScaffoldMessenger.of(context)
                            .showSnackBar(snackBar)
                            .closed
                            .then((_) {
                          _isSnackbarActive = false;
                        });
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
    ;
  }

  Widget noTrailer() {
    return const Center(child: Text("No trailer available"));
  }

  Widget trailerLayout(List<TrailerModel> trailers) {
    if (trailers.length > 1) {
      return Row(
        children: <Widget>[
          trailerItem(trailers, 0),
          trailerItem(trailers, 1),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          trailerItem(trailers, 0),
        ],
      );
    }
  }

  trailerItem(List<TrailerModel> trailers, int index) {
    return Expanded(
      child: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.all(5.0),
            height: 100.0,
            color: Colors.grey,
            child: const Center(child: Icon(Icons.play_circle_filled)),
          ),
          Text(
            trailers[index].name ?? '',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
