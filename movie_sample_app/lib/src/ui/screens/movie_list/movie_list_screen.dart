import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/movie_detail/movie_detail_bloc.dart';
import '../../../blocs/movie_list/movie_bloc.dart';
import '../../../models/movie.dart';
import '../../../repositories/movie_repository.dart';
import '../../components/movie_grid_item_widget.dart';
import '../../components/movie_item_widget.dart';
import '../movie_detail/movie_detail_screen.dart';

class MovieListScreen extends StatefulWidget {
  const MovieListScreen({super.key});

  @override
  State<MovieListScreen> createState() => _MovieListScreenState();
}

class _MovieListScreenState extends State<MovieListScreen> {
  final _scrollController = ScrollController();
  late MovieBloc bloc;
  bool isList = true;

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.offset) {
        bloc.add(const GetMovies());
      }
    });
    super.initState();
    bloc = context.read<MovieBloc>();
    bloc.add(const GetMovies());
  }

  Widget _buildMainView(
      BuildContext context, List<Movie> movies, bool hasMore) {
    int crossAxisCount = isList ? 1 : 2;
    double height = isList ? MediaQuery.of(context).size.width / 100 : 1;
    return Padding(
        padding: const EdgeInsets.all(10),
        child: CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverGrid(
              delegate: SliverChildBuilderDelegate((context, index) {
                return InkResponse(
                  enableFeedback: true,
                  child: GridTile(
                      child: isList
                          ? MovieItemWidget(movies[index])
                          : MovieGirdItemWidget(movies[index])),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return BlocProvider<MovieDetailBloc>(
                            create: (context) {
                              return MovieDetailBloc(DefaultMovieRepository());
                            },
                            child: MovieDetailScreen(movies[index]),
                          );
                        },
                      ),
                    );
                  },
                );
              }, childCount: movies.length),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: height,
                  crossAxisCount: crossAxisCount,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 5),
            ),
            SliverToBoxAdapter(
              child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: hasMore
                        ? const CircularProgressIndicator()
                        : const Text('You already reach max of items'),
                  )),
            )
          ],
        ));
  }

  Future<void> _onRefresh() async {
    bloc.add(const RefreshMovies());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Movies'),
          actions: [
            GestureDetector(
              child: Icon(isList ? Icons.list : Icons.grid_view),
              onTap: () {
                setState(() {
                  isList = !isList;
                });
              },
            )
          ],
        ),
        body: BlocConsumer<MovieBloc, MovieState>(
          builder: (context, state) {
            if (state is MovieInitial) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is MovieGetDataSuccess) {
              return RefreshIndicator(
                  onRefresh: _onRefresh,
                  child: _buildMainView(context, state.movies, state.hasMore));
            }
            return Container();
          },
          listener: (context, state) {
            if (state is MovieGetDataError) {
              final snackBar = SnackBar(content: Text(state.message));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
