import 'package:flutter/material.dart';

import '../../models/movie.dart';

class MovieGirdItemWidget extends StatelessWidget {
  final Movie movie;

  const MovieGirdItemWidget(this.movie, {super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.network(
          'https://image.tmdb.org/t/p/w154${(movie.posterPath ?? '')}',
          fit: BoxFit.fitWidth,
          width: double.maxFinite,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              movie.safeTitle,
              style: const TextStyle(
                  color: Colors.white, fontWeight: FontWeight.bold),
              textAlign: TextAlign.left,
            ),
            Row(
              children: [
                const Icon(
                  Icons.star,
                  color: Colors.orange,
                ),
                Text(
                  movie.rating.toString(),
                  style: const TextStyle(color: Colors.white),
                )
              ],
            )
          ],
        )
      ],
    );
  }
}
