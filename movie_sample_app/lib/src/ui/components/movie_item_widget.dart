import 'package:flutter/material.dart';

import '../../models/movie.dart';

class MovieItemWidget extends StatelessWidget {
  final Movie movie;

  const MovieItemWidget(this.movie, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      color: Colors.transparent,
      child: Row(
        children: [
          SizedBox(
            height: 80,
            child: Image(
              image: NetworkImage(
                  'https://image.tmdb.org/t/p/w154${(movie.posterPath ?? '')}'),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    movie.safeTitle,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(movie.releaseDate ?? ''),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.star,
                          color: Colors.orange,
                        ),
                        Text(movie.rating.toString())
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          const Icon(
            Icons.arrow_forward_ios,
            color: Colors.grey,
            size: 15,
          )
        ],
      ),
    );
  }
}
